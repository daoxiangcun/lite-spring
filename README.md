lite-spring模拟spring实现了一个轻量级的java bean框架，支持以下特性：
1. 通过xml配置bean
2. 通过注解自动配置bean
3. 支持bean的属性配置，包括primitive type和reference type
4. 实现了基本的aop功能，可基于此实现事务，日志管理等功能