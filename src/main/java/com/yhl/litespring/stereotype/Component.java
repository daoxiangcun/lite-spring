package com.yhl.litespring.stereotype;

import java.lang.annotation.*;

/**
 * Created by 44811 on 2018/7/29.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Component {
    String value() default "";
}
