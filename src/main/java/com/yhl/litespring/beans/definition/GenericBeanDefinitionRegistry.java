package com.yhl.litespring.beans.definition;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * Created by 44811 on 2018/6/17.
 */
public class GenericBeanDefinitionRegistry implements BeanDefinitionRegistry {
    private Map<String, BeanDefinition> idBeanDefinitionMap = Maps.newHashMap();

    public BeanDefinition getBeanDefinition(String beanId) {
        return idBeanDefinitionMap.get(beanId);
    }

    public void registerBeanDefinition(BeanDefinition bd) {
        idBeanDefinitionMap.put(bd.getId(), bd);
    }
}
