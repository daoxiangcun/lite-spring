package com.yhl.litespring.beans.definition;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * Created by yuhongliang on 18-6-15.
 */
public class BeanDefinition {
    private String id;
    private String classname;
    private String scope;
    private List<PropertyValue> propertyValues = Lists.newArrayList();
    private ConstructArgument constructArgument;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public List<PropertyValue> getPropertyValues() {
        return propertyValues;
    }

    public void setPropertyValues(List<PropertyValue> propertyValues) {
        this.propertyValues = propertyValues;
    }

    public ConstructArgument getConstructArgument() {
        return constructArgument;
    }

    public void setConstructArgument(ConstructArgument constructArgument) {
        this.constructArgument = constructArgument;
    }
}
