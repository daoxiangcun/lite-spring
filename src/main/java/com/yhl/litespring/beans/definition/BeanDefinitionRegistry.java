package com.yhl.litespring.beans.definition;

/**
 * Created by 44811 on 2018/6/17.
 */
public interface BeanDefinitionRegistry {
    BeanDefinition getBeanDefinition(String beanId);
    void registerBeanDefinition(BeanDefinition bd);
}
