package com.yhl.litespring.beans.definition;

import com.yhl.litespring.beans.factory.BeanFactory;
import com.yhl.litespring.beans.factory.config.RuntimeBeanReference;
import com.yhl.litespring.beans.factory.config.TypedStringValue;

/**
 * Created by 44811 on 2018/7/3.
 */
public class BeanDefinitionValueResolver {
    private BeanFactory factory;

    public BeanDefinitionValueResolver(BeanFactory factory) {
        this.factory = factory;
    }


    public Object resolve(Object value) {
        if (value instanceof RuntimeBeanReference) {
            String beanName = ((RuntimeBeanReference) value).getBeanName();
            return factory.getBean(beanName);
        } else if (value instanceof TypedStringValue){
            return ((TypedStringValue) value).getValue();
        } else {
            // TODO
            throw new RuntimeException("wrong value type!");
        }
    }
}
