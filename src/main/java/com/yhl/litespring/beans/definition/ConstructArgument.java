package com.yhl.litespring.beans.definition;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * bean defintion中的构造函数
 * Created by 44811 on 2018/7/16.
 */
public class ConstructArgument {
    private List<ValueHolder> argumentValues = Lists.newArrayList();

    public List<ValueHolder> getArgumentValues() {
        return argumentValues;
    }

    public void setArgumentValues(List<ValueHolder> argumentValues) {
        this.argumentValues = argumentValues;
    }

    public void addArgumentValue(ValueHolder valueHolder) {
        this.argumentValues.add(valueHolder);
    }

    public int getArgumentCount() {
        return argumentValues.size();
    }

    public static class ValueHolder {
        private String name;
        private String type;
        private Object value;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }
    }
}
