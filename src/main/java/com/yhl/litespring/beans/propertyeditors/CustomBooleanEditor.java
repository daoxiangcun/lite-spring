package com.yhl.litespring.beans.propertyeditors;

import com.google.common.base.Strings;
import com.yhl.litespring.utils.NumberUtils;

import java.beans.PropertyEditorSupport;

/**
 * Created by 44811 on 2018/7/4.
 */
public class CustomBooleanEditor extends PropertyEditorSupport {
    private boolean allowEmpty;

    public CustomBooleanEditor(boolean allowEmpty) {
        this.allowEmpty = allowEmpty;
    }

    @Override
    public void setAsText(String text) {
        if (allowEmpty && Strings.isNullOrEmpty(text)) {
            setValue(null);
        } else {
            if (text.equalsIgnoreCase("true") ||
                    text.equalsIgnoreCase("on") ||
                    text.equalsIgnoreCase("1") ||
                    text.equalsIgnoreCase("yes")) {
                setValue(Boolean.TRUE);
            } else if (text.equalsIgnoreCase("false") ||
                    text.equalsIgnoreCase("off") ||
                    text.equalsIgnoreCase("0") ||
                    text.equalsIgnoreCase("no")) {
                setValue(Boolean.FALSE);
            } else {
                throw new IllegalArgumentException("Invalid boolean value [" + text + "]");
            }
        }
    }

    @Override
    public String getAsText() {
        if (Boolean.TRUE.equals(getValue())) {
            return "true";
        } else if (Boolean.FALSE.equals(getValue())) {
            return "false";
        } else {
            return "";
        }
    }
}
