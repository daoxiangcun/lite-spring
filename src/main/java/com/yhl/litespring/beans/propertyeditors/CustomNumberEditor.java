package com.yhl.litespring.beans.propertyeditors;

import com.google.common.base.Strings;
import com.yhl.litespring.utils.NumberUtils;

import java.beans.PropertyEditorSupport;

/**
 * Created by 44811 on 2018/7/4.
 */
public class CustomNumberEditor extends PropertyEditorSupport {
    private boolean allowEmpty;
    private Class<? extends Number> targetClass;

    public CustomNumberEditor(Class<? extends Number> targetClass, boolean allowEmpty) {
        if (targetClass == null || !Number.class.isAssignableFrom(targetClass)) {
            throw new IllegalArgumentException("targetclass must be a subclass of Number");
        }
        this.targetClass = targetClass;
        this.allowEmpty = allowEmpty;
    }

    @Override
    public void setAsText(String text) {
        if (allowEmpty && Strings.isNullOrEmpty(text)) {
            setValue(null);
        } else {
            setValue(NumberUtils.parseNumber(text, this.targetClass));
        }
    }

    public void setValue(Object value) {
        if (value instanceof Number) {
            super.setValue(NumberUtils.convertNumberToTargetClass((Number) value, targetClass));
        } else {
            super.setValue(value);
        }
    }
}
