package com.yhl.litespring.beans.factory.config;

/**
 * Created by 44811 on 2018/7/1.
 */
public class RuntimeBeanReference {
    private String beanName;

    public RuntimeBeanReference(String beanName) {
        this.beanName = beanName;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }
}
