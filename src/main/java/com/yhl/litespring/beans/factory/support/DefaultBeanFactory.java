package com.yhl.litespring.beans.factory.support;

import com.google.common.base.Strings;
import com.yhl.litespring.beans.ConstructorResolver;
import com.yhl.litespring.beans.SimpleTypeConverter;
import com.yhl.litespring.beans.definition.*;
import com.yhl.litespring.beans.exception.BeanCreateException;
import com.yhl.litespring.beans.exception.BeanDefinitionException;
import com.yhl.litespring.beans.exception.TypeMismatchException;
import com.yhl.litespring.beans.factory.BeanFactory;
import com.yhl.litespring.beans.factory.xml.XmlBeanDefinitionReader;
import com.yhl.litespring.core.io.Resource;
import com.yhl.litespring.utils.ClassUtils;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by yuhongliang on 2018/6/17.
 */
public class DefaultBeanFactory implements BeanFactory {
    private BeanDefinitionRegistry beanDefinitionRegistry = new GenericBeanDefinitionRegistry();
    private XmlBeanDefinitionReader beanDefinitionReader = new XmlBeanDefinitionReader(beanDefinitionRegistry);
    private Map<String, Object> beanIdObjectMap = new ConcurrentHashMap<String, Object>();
    private static final String SCOPE_SINGLETON = "singleton";
    private static final String SCOPE_PROTOTYPE = "prototype";

    public DefaultBeanFactory(Resource resource) {
        beanDefinitionReader.loadXmlBeanDefinition(resource);
    }

    public Object getBean(String beanId) {
        BeanDefinition bd = beanDefinitionRegistry.getBeanDefinition(beanId);
        if (bd == null) {
            throw new BeanDefinitionException("bean with id " + beanId + " not exist!");
        }
        String scope = bd.getScope();
        if(Strings.isNullOrEmpty(scope)) {
            scope = SCOPE_SINGLETON;        // 默认为singleton
        }
        if (beanIdObjectMap.containsKey(beanId) && scope.equals(SCOPE_SINGLETON)) {
            // 返回已有对象
            return beanIdObjectMap.get(beanId);
        } else {
            Object object = createBean(bd);
            beanIdObjectMap.put(beanId, object);
            return object;
        }
    }

    public BeanDefinition getBeanDefinition(String beanId) {
        return beanDefinitionRegistry.getBeanDefinition(beanId);
    }

    // 创建一个新的对象
    private Object createBean(BeanDefinition bd) {
        Object bean = instantiateBean(bd);
        try {
            populateBean(bd, bean);
        } catch (TypeMismatchException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return bean;
    }

    private Object instantiateBean(BeanDefinition bd) {
        try {
            Class<?> aClass = ClassUtils.loadClass(bd.getClassname());
            ConstructArgument ca = bd.getConstructArgument();
            if (ca != null && ca.getArgumentCount() > 0) {
                // 有构造函数，选择合适的构造函数
                ConstructorResolver resolver = new ConstructorResolver(this);
                return resolver.autowireConstructor(bd);
            } else {
                return aClass.newInstance();
            }
        } catch (Exception e) {
            throw new BeanCreateException("create bean of class" + bd.getClassname() + " failed!");
        }
    }

    /**
     * 设置属性
     * @param bd
     * @param bean
     */
    private void populateBean(BeanDefinition bd, Object bean) throws IntrospectionException, InvocationTargetException, IllegalAccessException, TypeMismatchException {
        List<PropertyValue> propertyValueList = bd.getPropertyValues();
        if (propertyValueList != null && !propertyValueList.isEmpty()) {
            BeanDefinitionValueResolver resolver = new BeanDefinitionValueResolver(this);
            SimpleTypeConverter converter = new SimpleTypeConverter();
            for (PropertyValue pv : propertyValueList) {
                String propertyName = pv.getName();
                Object originValue = pv.getValue();
                Object resolvedValue = resolver.resolve(originValue);
                // 设置bean的属性
                BeanInfo beanInfo = Introspector.getBeanInfo(bean.getClass());
                PropertyDescriptor[] pds = beanInfo.getPropertyDescriptors();
                for (PropertyDescriptor pd : pds) {
                    if (pd.getName().equals(propertyName)) {
                        Object convertValue = converter.convertIfNecessary(resolvedValue, pd.getPropertyType());
                        pd.getWriteMethod().invoke(bean, convertValue);
                        break;
                    }
                }
            }
        }
    }
}
