package com.yhl.litespring.beans.factory.config;

/**
 * Created by 44811 on 2018/7/1.
 */
public class TypedStringValue {
    private String value;

    public TypedStringValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
