package com.yhl.litespring.beans.factory.annotation;

import java.lang.annotation.*;

/**
 * Created by 44811 on 2018/7/29.
 */
@Target({ElementType.FIELD, ElementType.CONSTRUCTOR, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Autowried {
    boolean required() default true;
}
