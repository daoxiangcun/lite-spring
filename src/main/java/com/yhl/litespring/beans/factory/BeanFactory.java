package com.yhl.litespring.beans.factory;

/**
 * Created by yuhongliang on 18-6-15.
 */
public interface BeanFactory {
    Object getBean(String id);
}
