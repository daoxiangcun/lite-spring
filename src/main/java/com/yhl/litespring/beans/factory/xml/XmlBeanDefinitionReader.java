package com.yhl.litespring.beans.factory.xml;

import com.google.common.base.Strings;
import com.yhl.litespring.beans.definition.BeanDefinition;
import com.yhl.litespring.beans.definition.BeanDefinitionRegistry;
import com.yhl.litespring.beans.definition.ConstructArgument;
import com.yhl.litespring.beans.definition.PropertyValue;
import com.yhl.litespring.beans.exception.BeanDefinitionException;
import com.yhl.litespring.beans.factory.config.RuntimeBeanReference;
import com.yhl.litespring.beans.factory.config.TypedStringValue;
import com.yhl.litespring.core.io.Resource;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.IOException;
import java.util.Iterator;

/**
 * Created by 44811 on 2018/6/17.
 */
public class XmlBeanDefinitionReader {
    private static final String ATTRIBUTE_ID = "id";
    private static final String ATTRIBUTE_NAME = "name";
    private static final String ATTRIBUTE_VALUE = "value";
    private static final String ATTRIBUTE_TYPE = "type";
    private static final String ATTRIBUTE_REF = "ref";
    private static final String ATTRIBUTE_CLASS = "class";
    private static final String ATTRIBUTE_SCOPE = "scope";
    private static final String ELEMENT_PROPERTY = "property";
    private static final String ELEMENT_CONSTRUCT_ARG = "construct-arg";

    private BeanDefinitionRegistry bdRegistry;

    public XmlBeanDefinitionReader(BeanDefinitionRegistry registry) {
        this.bdRegistry = registry;
    }

    public void loadXmlBeanDefinition(Resource resource) {
        try {
            // 创建saxReader对象
            SAXReader reader = new SAXReader();
            // 通过read方法读取一个文件 转换成Document对象
            Document document = reader.read(resource.getInputStream());
            //获取根节点元素对象
            Element xmlRoot = document.getRootElement();
            Iterator<Element> it = xmlRoot.elementIterator();
            while(it.hasNext()) {
                Element element = it.next();
                BeanDefinition bd = parseBeanDefinition(element);
                bdRegistry.registerBeanDefinition(bd);
            }
        } catch (DocumentException e) {
            throw new BeanDefinitionException(e);
        } catch (IOException e) {
            throw new BeanDefinitionException(e);
        }
    }

    private BeanDefinition parseBeanDefinition(Element element) throws DocumentException {
        BeanDefinition bd = new BeanDefinition();
        String beanId = element.attributeValue(ATTRIBUTE_ID);
        String beanClassName = element.attributeValue(ATTRIBUTE_CLASS);
        String scope = element.attributeValue(ATTRIBUTE_SCOPE);
        bd.setId(beanId);
        bd.setClassname(beanClassName);
        bd.setScope(scope);
        parsePropertyElement(element, bd);
        parseConstructArgElement(element, bd);
        return bd;
    }

    // 解析构造函数
    private void parseConstructArgElement(Element beanElement, BeanDefinition bd) {
        Iterator<Element> constructArgIter = beanElement.elementIterator(ELEMENT_CONSTRUCT_ARG);
        if (constructArgIter != null) {
            ConstructArgument ca = new ConstructArgument();
            while(constructArgIter.hasNext()) {
                Element oneArg = constructArgIter.next();
                Object value = parsePropertyValue(oneArg, "");
                ConstructArgument.ValueHolder vh = new ConstructArgument.ValueHolder();
                vh.setValue(value);
                boolean hasName = (oneArg.attribute(ATTRIBUTE_NAME) != null);
                boolean hasType = (oneArg.attribute(ATTRIBUTE_TYPE) != null);
                if (hasName) {
                    vh.setName(oneArg.attributeValue(ATTRIBUTE_NAME));
                }
                if(hasType) {
                    vh.setType(oneArg.attributeValue(ATTRIBUTE_TYPE));
                }
                ca.addArgumentValue(vh);
            }
            bd.setConstructArgument(ca);
        }
    }

    private void parsePropertyElement(Element beanElement, BeanDefinition bd) {
        Iterator<Element> propIter = beanElement.elementIterator(ELEMENT_PROPERTY);
        if (propIter != null) {
            while(propIter.hasNext()) {
                Element prop = propIter.next();
                PropertyValue pv = new PropertyValue();
                String propName = prop.attributeValue(ATTRIBUTE_NAME);
                pv.setName(propName);
                Object value = parsePropertyValue(prop, propName);
                pv.setValue(value);
                bd.getPropertyValues().add(pv);
            }
        }
    }

    private Object parsePropertyValue(Element propElem, String propName) {
        boolean hasRef = (propElem.attribute(ATTRIBUTE_REF) != null);
        boolean hasValue = (propElem.attribute(ATTRIBUTE_VALUE) != null);
        if (hasRef) {
            String beanName = propElem.attributeValue(ATTRIBUTE_REF);
            if (Strings.isNullOrEmpty(beanName)) {
                throw new RuntimeException("ref value is empty!");
            }
            RuntimeBeanReference rbr = new RuntimeBeanReference(beanName);
            return rbr;
        } else if (hasValue) {
            String value = propElem.attributeValue(ATTRIBUTE_VALUE);
            TypedStringValue tv = new TypedStringValue(value);
            return tv;
        } else {
            throw new RuntimeException(propName + "must has ref or value attribute");
        }
    }
}
