package com.yhl.litespring.beans.exception;

/**
 * Bean未定义异常
 * Created by 44811 on 2018/6/17.
 */
public class BeanDefinitionException extends BeanException {
    public BeanDefinitionException(String msg) {
        super(msg);
    }
    public BeanDefinitionException(Throwable cause) {
        super(cause);
    }
}
