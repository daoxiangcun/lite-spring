package com.yhl.litespring.beans.exception;

/**
 * Bean创建失败异常
 * Created by 44811 on 2018/6/17.
 */
public class BeanCreateException extends BeanException {
    public BeanCreateException(String msg) {
        super(msg);
    }

    public BeanCreateException(Throwable cause) {
        super(cause);
    }
}
