package com.yhl.litespring.beans.exception;

/**
 * Created by 44811 on 2018/7/8.
 */
public class TypeMismatchException extends Throwable {
    private Object value;
    private Class<?> requiredType;

    public <T> TypeMismatchException(Object value, Class<T> requiredType) {
        super("Failed to convert value :"+value + "to type "+requiredType);
        this.value = value;
        this.requiredType = requiredType;
    }

    public Object getValue() {
        return value;
    }

    public Class<?> getRequiredType() {
        return requiredType;
    }

}
