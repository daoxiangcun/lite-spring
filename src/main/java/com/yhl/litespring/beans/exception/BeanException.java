package com.yhl.litespring.beans.exception;

/**
 * Created by 44811 on 2018/6/17.
 */
public class BeanException extends RuntimeException {
    public BeanException(String message) {
        super(message);
    }

    public BeanException(Throwable cause) {
        super(cause);
    }
}
