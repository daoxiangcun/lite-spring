package com.yhl.litespring.beans;

import com.yhl.litespring.beans.exception.TypeMismatchException;

/**
 * Created by 44811 on 2018/7/8.
 */
public interface TypeConverter {
    <T> T convertIfNecessary(Object value, Class<T> targetClass) throws TypeMismatchException;
}
