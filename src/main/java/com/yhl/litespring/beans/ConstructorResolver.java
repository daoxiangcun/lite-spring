package com.yhl.litespring.beans;

import com.yhl.litespring.beans.definition.BeanDefinition;
import com.yhl.litespring.beans.definition.BeanDefinitionValueResolver;
import com.yhl.litespring.beans.definition.ConstructArgument;
import com.yhl.litespring.beans.exception.BeanCreateException;
import com.yhl.litespring.beans.exception.TypeMismatchException;
import com.yhl.litespring.beans.factory.BeanFactory;
import com.yhl.litespring.utils.ClassUtils;

import java.lang.reflect.Constructor;
import java.util.List;

/**
 * Created by 44811 on 2018/7/16.
 */
public class ConstructorResolver {
    private BeanFactory beanFactory;

    public ConstructorResolver(BeanFactory factory) {
        this.beanFactory = factory;
    }

    public Object autowireConstructor(BeanDefinition bd) throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        String className = bd.getClassname();
        Class<?> beanClass = ClassUtils.loadClass(className);
        Constructor<?>[] allConstructors = beanClass.getConstructors();
        BeanDefinitionValueResolver resolver = new BeanDefinitionValueResolver(beanFactory);
        ConstructArgument contructArg = bd.getConstructArgument();
        SimpleTypeConverter converter = new SimpleTypeConverter();
        Object[] argsToUse = new Object[contructArg.getArgumentCount()];
        Constructor<?> constructorToUse = null;
        for (Constructor<?> one : allConstructors) {
            Class<?>[] paramTypes = one.getParameterTypes();
            if (contructArg.getArgumentCount() != paramTypes.length) {
                // 构造函数个数不对，跳过
                continue;
            }
            // 判断类型是否正确
            boolean rightConstructor = valuesMatchTypes(contructArg.getArgumentValues(), paramTypes, argsToUse, resolver, converter);
            if (rightConstructor) {
                constructorToUse = one;
                break;
            }
        }
        if (constructorToUse == null) {
            throw new BeanCreateException(bd.getId() + " cannot find a suitable constructor");
        }
        try {
            return constructorToUse.newInstance(argsToUse);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BeanCreateException(bd.getId() + " constructor newInstance exception");
        }
    }

    private boolean valuesMatchTypes(List<ConstructArgument.ValueHolder> valueHolders, Class<?>[] paramTypes, Object[] argsToUse, BeanDefinitionValueResolver resolver, SimpleTypeConverter converter) {
        for (int i = 0; i < paramTypes.length; i++) {
            Object originValue = valueHolders.get(i).getValue(); // originValue表示RuntimeBeanReference, TypedStringValue
            Object resolvedValue = resolver.resolve(originValue);
            try {
                Object convertValue = converter.convertIfNecessary(resolvedValue, paramTypes[i]);
                argsToUse[i] = convertValue;
            } catch (TypeMismatchException e) {
                return false;
            }
        }
        return true;
    }
}
