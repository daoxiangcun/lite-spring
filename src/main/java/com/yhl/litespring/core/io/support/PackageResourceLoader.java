package com.yhl.litespring.core.io.support;

import com.yhl.litespring.core.io.FileSystemResource;
import com.yhl.litespring.core.io.Resource;
import com.yhl.litespring.utils.ClassUtils;
import com.yhl.litespring.utils.FileUtils;

import java.io.File;
import java.util.List;

/**
 * Created by 44811 on 2018/7/29.
 */
public class PackageResourceLoader {
    /**
     * 根据包名得到下面的文件
     * @param basepackage
     * @return
     */
    public Resource[] getResources(String basepackage) {
        String path = ClassUtils.convertClassNameToResourcePath(basepackage);
        File rootDir = new File(path);
        List<File> fileList = FileUtils.listFilesRecurse(rootDir);
        Resource[] resources = new Resource[fileList.size()];
        int i = 0;
        for (File file : fileList) {
            resources[i++] = new FileSystemResource(file.getAbsolutePath());
        }
        return resources;
    }
}
