package com.yhl.litespring.core.io;

import com.yhl.litespring.utils.ClassUtils;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by 44811 on 2018/6/17.
 */
public class ClassPathResource implements Resource {
    private String xmlFileName;
    private ClassLoader classLoader;

    public ClassPathResource(String xmlFileName) {
        this(xmlFileName, null);
    }

    public ClassPathResource(String xmlFileName, ClassLoader classLoader) {
        this.xmlFileName = xmlFileName;
        this.classLoader = classLoader == null ? ClassUtils.getDefaultClassLoader() : classLoader;
    }

    public InputStream getInputStream() throws IOException {
        return classLoader.getResourceAsStream(xmlFileName);
    }
}