package com.yhl.litespring.core.io;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by 44811 on 2018/6/17.
 */
public interface Resource {
    InputStream getInputStream() throws IOException;
}
