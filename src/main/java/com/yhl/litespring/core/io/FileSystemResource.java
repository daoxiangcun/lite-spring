package com.yhl.litespring.core.io;

import java.io.*;

/**
 * Created by 44811 on 2018/6/17.
 */
public class FileSystemResource implements Resource {
    private String filepath;

    public FileSystemResource(String filepath) {
        this.filepath = filepath;
    }

    public InputStream getInputStream() throws IOException {
        File file = new File(filepath);
        BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(file));
        return inputStream;
    }
}
