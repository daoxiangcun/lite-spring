package com.yhl.litespring.core.type.classreading;

import com.yhl.litespring.beans.factory.annotation.AnnotationAttributes;
import org.springframework.asm.AnnotationVisitor;
import org.springframework.asm.SpringAsmInfo;

import java.lang.annotation.Annotation;
import java.util.HashMap;

/**
 * Created by 44811 on 2018/7/29.
 */
public class AnnotationAttributesReadingVisitor extends AnnotationVisitor {
    private AnnotationAttributes annotationAttributes = new AnnotationAttributes();
    private String annotationClassName;
    private HashMap<String, AnnotationAttributes> attributesHashMap;

    public AnnotationAttributesReadingVisitor(String annotationClassName, HashMap<String, AnnotationAttributes> attributesHashMap) {
        super(SpringAsmInfo.ASM_VERSION);
        this.annotationClassName = annotationClassName;
        this.attributesHashMap = attributesHashMap;
    }

    public void visit(String attributeName, Object attributeValue) {
        annotationAttributes.put(attributeName, attributeValue);
    }

    public void visitEnd() {
        this.attributesHashMap.put(this.annotationClassName, this.annotationAttributes);
    }
}
