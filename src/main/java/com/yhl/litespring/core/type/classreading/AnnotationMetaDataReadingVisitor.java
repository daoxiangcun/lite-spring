package com.yhl.litespring.core.type.classreading;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.yhl.litespring.beans.factory.annotation.AnnotationAttributes;
import org.springframework.asm.AnnotationVisitor;
import org.springframework.asm.Type;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by 44811 on 2018/7/29.
 */
public class AnnotationMetaDataReadingVisitor extends ClassMetaDataReadingVisitor{
    private HashSet<String> annotationSet = Sets.newHashSet();
    private HashMap<String, AnnotationAttributes> annotationMap = Maps.newHashMap();

    public AnnotationMetaDataReadingVisitor() {
    }

    @Override
    public AnnotationVisitor visitAnnotation(String desc, boolean visiable) {
        String annotationClassName = Type.getType(desc).getClassName();
        annotationSet.add(annotationClassName);
        return new AnnotationAttributesReadingVisitor(annotationClassName, this.annotationMap);
    }

    public Set<String> getAnnotationTypes() {
        return annotationSet;
    }

    public boolean hasAnnotation(String annotation) {
        return annotationMap.containsKey(annotation);
    }

    public AnnotationAttributes getAnnotationAttributes(String annotation) {
        return annotationMap.get(annotation);
    }
}
