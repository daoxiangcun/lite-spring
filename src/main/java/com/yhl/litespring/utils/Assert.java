package com.yhl.litespring.utils;

/**
 * Created by 44811 on 2018/7/8.
 */
public abstract class Assert {
    public static void notNull(String message, Object object) {
        if (object == null) {
            throw new IllegalArgumentException(message);
        }
    }
}