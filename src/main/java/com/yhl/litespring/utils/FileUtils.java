package com.yhl.litespring.utils;

import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;
import com.google.common.io.Files;

import java.io.File;
import java.util.List;

/**
 * Created by 44811 on 2018/7/29.
 */
public class FileUtils {
    public static List<File> listFilesRecurse(File dir) {
        FluentIterable<File> iterable = Files.fileTreeTraverser().breadthFirstTraversal(dir).filter(new Predicate<File>(){
            public boolean apply(File input) {
                return input.isFile();
            }
        });
        List<File> files = Lists.newArrayList();
        for (File one : iterable) {
            files.add(one);
        }
        return files;
    }
}
