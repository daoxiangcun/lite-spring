package com.yhl.litespring.context;

import com.yhl.litespring.beans.factory.BeanFactory;

/**
 * Created by yuhongliang on 2018/6/17.
 */
public interface ApplicationContext extends BeanFactory {
}
