package com.yhl.litespring.context.support;

import com.yhl.litespring.beans.factory.support.DefaultBeanFactory;
import com.yhl.litespring.core.io.FileSystemResource;

/**
 * Created by 44811 on 2018/6/20.
 */
public class FileSystemXmlApplicationContext extends AbsApplicationContext {
    public FileSystemXmlApplicationContext(String filepath) {
        FileSystemResource resource = new FileSystemResource(filepath);
        DefaultBeanFactory factory = new DefaultBeanFactory(resource);
        setFactory(factory);
    }
}
