package com.yhl.litespring.context.support;

import com.yhl.litespring.beans.exception.BeanCreateException;
import com.yhl.litespring.beans.factory.support.DefaultBeanFactory;
import com.yhl.litespring.context.ApplicationContext;

/**
 * Created by 44811 on 2018/6/20.
 */
public abstract class AbsApplicationContext implements ApplicationContext {
    private DefaultBeanFactory factory = null;

    public Object getBean(String beanId) {
        if (factory != null) {
            return factory.getBean(beanId);
        } else {
            throw new BeanCreateException("bean factory is null");
        }
    }

    public void setFactory(DefaultBeanFactory factory) {
        this.factory = factory;
    }
}
