package com.yhl.litespring.context.support;

import com.yhl.litespring.beans.factory.support.DefaultBeanFactory;
import com.yhl.litespring.core.io.ClassPathResource;

/**
 * Created by yuhongliang on 2018/6/17.
 */
public class ClassPathXmlApplicationContext extends AbsApplicationContext {
    public ClassPathXmlApplicationContext(String xmlFileName) {
        ClassPathResource resource = new ClassPathResource(xmlFileName);
        DefaultBeanFactory factory = new DefaultBeanFactory(resource);
        setFactory(factory);
    }
}
