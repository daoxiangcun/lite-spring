package com.yhl.litespring.test.test;

import com.yhl.litespring.beans.definition.*;
import com.yhl.litespring.beans.exception.BeanDefinitionException;
import com.yhl.litespring.beans.factory.config.RuntimeBeanReference;
import com.yhl.litespring.beans.factory.config.TypedStringValue;
import com.yhl.litespring.beans.factory.xml.XmlBeanDefinitionReader;
import com.yhl.litespring.core.io.FileSystemResource;
import org.dom4j.DocumentException;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created by 44811 on 2018/6/17.
 */
public class BeanDefinitionTest extends BaseTest {
    private BeanDefinitionRegistry beanDefinitionRegistry = new GenericBeanDefinitionRegistry();
    private XmlBeanDefinitionReader beanDefinitionReader = new XmlBeanDefinitionReader(beanDefinitionRegistry);

    @Test
    public void testGetBeanDefinition() throws DocumentException {
        String xmlFile = getFilePath("calculator.xml");
        FileSystemResource resource = new FileSystemResource(xmlFile);
        beanDefinitionReader.loadXmlBeanDefinition(resource);
        BeanDefinition beanDefinition = beanDefinitionRegistry.getBeanDefinition("calc");
        Assert.assertEquals("com.yhl.litespring.test.beans.service.Calculator", beanDefinition.getClassname());
    }

    /**
     * 与testGetBeanDefinition相比，增加了PropertyValue的测试
     * @throws DocumentException
     */
    @Test
    public void testGetBeanDefinition2() throws DocumentException {
        String xmlFile = getFilePath("miaosha.xml");
        FileSystemResource resource = new FileSystemResource(xmlFile);
        beanDefinitionReader.loadXmlBeanDefinition(resource);
        BeanDefinition beanDefinition = beanDefinitionRegistry.getBeanDefinition("goodsService");
        Assert.assertEquals("com.yhl.litespring.test.beans.service.GoodsService", beanDefinition.getClassname());

        List<PropertyValue> propertyValueList = beanDefinition.getPropertyValues();
        Assert.assertEquals(3, propertyValueList.size());
        PropertyValue goodsPV = getProperty(propertyValueList, "goodsDAO");
        Assert.assertNotNull(goodsPV);

        PropertyValue invalidPV = getProperty(propertyValueList, "invalidPV");
        Assert.assertNull(invalidPV);
    }

    private PropertyValue getProperty(List<PropertyValue> propertyValueList, String pvName) {
        for (PropertyValue pv : propertyValueList) {
            if (pv.getName().equals(pvName)) {
                return pv;
            }
        }
        return null;
    }

    @Test
    public void testInvalidXml() {
        try{
            String invalidFile = getFilePath("xxx.xml");
            FileSystemResource resource = new FileSystemResource(invalidFile);
            beanDefinitionReader.loadXmlBeanDefinition(resource);
        } catch (BeanDefinitionException e) {
            System.out.println("load xml bean definition exception");
            return;
        } catch (Exception e) {
            System.out.println("exception happen");
            return;
        }
        Assert.fail("should not go to here");
    }

    @Test
    public void testGetBeanDefinition3() throws DocumentException {
        String xmlFile = getFilePath("miaosha2.xml");
        FileSystemResource resource = new FileSystemResource(xmlFile);
        beanDefinitionReader.loadXmlBeanDefinition(resource);
        BeanDefinition beanDefinition = beanDefinitionRegistry.getBeanDefinition("goodsService");
        Assert.assertEquals("com.yhl.litespring.test.beans.service.GoodsService", beanDefinition.getClassname());

        ConstructArgument constructArgument = beanDefinition.getConstructArgument();
        List<ConstructArgument.ValueHolder> valueHolders = constructArgument.getArgumentValues();
        Assert.assertEquals(3, valueHolders.size());

        RuntimeBeanReference arg1 = (RuntimeBeanReference)valueHolders.get(0).getValue();
        Assert.assertEquals("gooddao", arg1.getBeanName());

        TypedStringValue arg2 = (TypedStringValue)valueHolders.get(1).getValue();
        Assert.assertEquals("beijing", arg2.getValue());
    }
}
