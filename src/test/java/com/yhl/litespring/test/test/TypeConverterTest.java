package com.yhl.litespring.test.test;

import com.yhl.litespring.beans.SimpleTypeConverter;
import com.yhl.litespring.beans.TypeConverter;
import com.yhl.litespring.beans.exception.TypeMismatchException;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by 44811 on 2018/7/8.
 */
public class TypeConverterTest {
    @Test
    public void convertStringToInt() {
        TypeConverter converter = new SimpleTypeConverter();
        Integer intvalue = null;
        try {
            intvalue = converter.convertIfNecessary("3", Integer.class);
            Assert.assertNotNull(intvalue);
            Assert.assertEquals(3, intvalue.intValue());
        } catch (TypeMismatchException e) {
            Assert.fail();
        }
    }

    @Test
    public void convertStringToBoolean() {
        TypeConverter converter = new SimpleTypeConverter();
        Boolean boolValue = null;
        try {
            boolValue = converter.convertIfNecessary("true", Boolean.class);
            Assert.assertNotNull(boolValue);
            Assert.assertTrue(boolValue);
        } catch (TypeMismatchException e) {
            Assert.fail();
        }
    }
}
