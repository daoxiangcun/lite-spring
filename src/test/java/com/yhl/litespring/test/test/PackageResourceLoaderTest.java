package com.yhl.litespring.test.test;

import com.yhl.litespring.core.io.support.PackageResourceLoader;
import com.yhl.litespring.core.io.Resource;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by 44811 on 2018/7/29.
 */
public class PackageResourceLoaderTest {
    @Test
    public void testGetResource() {
        PackageResourceLoader loader = new PackageResourceLoader();
        Resource[] resources = loader.getResources("com.yhl.litespring.test");
        Assert.assertEquals(2, resources.length);
    }
}
