package com.yhl.litespring.test.test;

import com.yhl.litespring.utils.ClassUtils;
import com.yhl.litespring.utils.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.List;

/**
 * Created by 44811 on 2018/7/29.
 */
public class UtilsTest extends BaseTest{
    @Test
    public void testListFiles() {
        String folderPath = "D:\\yhl\\cygwin\\home\\44811\\opensource\\lite-spring\\src\\test\\resources";
        List<File> files = FileUtils.listFilesRecurse(new File(folderPath));
        Assert.assertEquals(5, files.size());
    }

    @Test
    public void testConvertclassNameToResourcePath() {
        String basePackage = "com.yhl.litespring.test";
        String convertedPath = ClassUtils.convertClassNameToResourcePath(basePackage);
    }
}
