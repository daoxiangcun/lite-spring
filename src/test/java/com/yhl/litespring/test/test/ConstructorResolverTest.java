package com.yhl.litespring.test.test;

import com.yhl.litespring.beans.ConstructorResolver;
import com.yhl.litespring.beans.definition.BeanDefinition;
import com.yhl.litespring.beans.definition.BeanDefinitionRegistry;
import com.yhl.litespring.beans.definition.GenericBeanDefinitionRegistry;
import com.yhl.litespring.beans.factory.support.DefaultBeanFactory;
import com.yhl.litespring.beans.factory.xml.XmlBeanDefinitionReader;
import com.yhl.litespring.core.io.ClassPathResource;
import com.yhl.litespring.test.beans.service.GoodsService;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by 44811 on 2018/7/16.
 */
public class ConstructorResolverTest extends BaseTest{
    private BeanDefinitionRegistry beanDefinitionRegistry = new GenericBeanDefinitionRegistry();
    private XmlBeanDefinitionReader beanDefinitionReader = new XmlBeanDefinitionReader(beanDefinitionRegistry);

    @Test
    public void testConstructor() throws IllegalAccessException, ClassNotFoundException, InstantiationException {
        ClassPathResource resource = new ClassPathResource("miaosha2.xml");
        DefaultBeanFactory factory = new DefaultBeanFactory(resource);
        ConstructorResolver resolver = new ConstructorResolver(factory);
        BeanDefinition beanDefinition = factory.getBeanDefinition("goodsService");
        Assert.assertEquals("com.yhl.litespring.test.beans.service.GoodsService", beanDefinition.getClassname());

        GoodsService goodsService = (GoodsService)resolver.autowireConstructor(beanDefinition);
        Assert.assertNotNull(goodsService.getGoodsDAO());
        Assert.assertEquals(1, goodsService.getTotalGoods());
        Assert.assertEquals("beijing", goodsService.getWareHouse());
    }
}
