package com.yhl.litespring.test.test;

import com.yhl.litespring.context.ApplicationContext;
import com.yhl.litespring.context.support.ClassPathXmlApplicationContext;
import com.yhl.litespring.test.beans.service.Student;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by 44811 on 2018/6/17.
 */
public class ApplicationContextTest extends BaseTest {
    @Test
    public void test() {
        ApplicationContext appContext = new ClassPathXmlApplicationContext("calculator.xml");
        Object calc = appContext.getBean("calc");
        Assert.assertNotNull(calc);
    }

    @Test
    public void testScopPrototype() {
//        String calcFile = getFilePath("calculator.xml");
        ApplicationContext appContext = new ClassPathXmlApplicationContext("calculator.xml");
        Object calc1 = appContext.getBean("calc");
        Object calc2 = appContext.getBean("calc");
        // calc的scope为singleton,所以calc2应该等于calc2
        Assert.assertEquals(calc1, calc2);

        // student的scope为prototype,所以student1!=student2
        Student student1 = (Student)appContext.getBean("student");
        int count1 = student1.getCount();
        Assert.assertEquals(count1, 1);

        Student student2 = (Student)appContext.getBean("student");
        int count2 = student1.getCount();
        Assert.assertEquals(count2, 2);

        Assert.assertNotEquals(student1, student2);
    }
}
