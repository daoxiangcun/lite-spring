package com.yhl.litespring.test.test;

import com.yhl.litespring.beans.propertyeditors.CustomBooleanEditor;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by 44811 on 2018/7/8.
 */
public class CustomBooleanTest {
    @Test
    public void testConverString() {
        CustomBooleanEditor editor = new CustomBooleanEditor(true);
        editor.setAsText("true");
        Object value = editor.getValue();
        Assert.assertTrue(value instanceof Boolean);
        Assert.assertEquals(Boolean.TRUE, value);

        editor.setAsText("");
        Assert.assertTrue(editor.getValue() == null);

        try {
            editor.setAsText("aaa");
        } catch (Exception e) {
            return;
        }
        Assert.fail();
    }
}
