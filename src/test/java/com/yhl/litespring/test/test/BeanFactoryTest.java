package com.yhl.litespring.test.test;

import com.yhl.litespring.beans.exception.BeanCreateException;
import com.yhl.litespring.beans.exception.BeanDefinitionException;
import com.yhl.litespring.beans.factory.BeanFactory;
import com.yhl.litespring.beans.factory.support.DefaultBeanFactory;
import com.yhl.litespring.core.io.ClassPathResource;
import com.yhl.litespring.test.beans.dao.GoodsDAO;
import com.yhl.litespring.test.beans.service.Calculator;
import com.yhl.litespring.test.beans.service.GoodsService;
import com.yhl.litespring.utils.ClassUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by yuhongliang on 18-6-15.
 */
public class BeanFactoryTest {
    private static String xmlFile;

    @BeforeClass
    public static void setup() {
//        ClassLoader cl = ClassUtils.getDefaultClassLoader();
        xmlFile = "calculator.xml";
        ///xmlFile = cl.getResource("calculator.xml").getFile();
    }

    @Test
    public void testGetBean() {
        ClassPathResource resource = new ClassPathResource(xmlFile);
        BeanFactory factory = new DefaultBeanFactory(resource);
        Calculator calculator = (Calculator) factory.getBean("calc");
        Assert.assertNotNull(calculator);
    }

    @Test
    public void testInvalidBean() {
        ClassPathResource resource = new ClassPathResource(xmlFile);
        BeanFactory factory = new DefaultBeanFactory(resource);
        try{
            factory.getBean("invalid");
        } catch (BeanCreateException e) {
            System.out.printf("bean create exception");
            return;
        } catch (BeanDefinitionException e) {
            System.out.println("bean definition exception");
            return;
        }
        Assert.fail("should not go to here");
    }

    @Test
    public void testGetBean2() {
        ClassPathResource resource = new ClassPathResource("miaosha.xml");
        BeanFactory factory = new DefaultBeanFactory(resource);
        GoodsService goodsService = (GoodsService) factory.getBean("goodsService");
        Assert.assertNotNull(goodsService);
        GoodsDAO goodsDAO = goodsService.getGoodsDAO();
        Assert.assertNotNull(goodsDAO);
        String wareHouse = goodsService.getWareHouse();
        Assert.assertEquals(wareHouse, "beijing");
        int totalGoods = goodsService.getTotalGoods();
        Assert.assertEquals(500, totalGoods);       // 验证TypeConverter工作是否正常
    }
}
