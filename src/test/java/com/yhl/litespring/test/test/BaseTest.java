package com.yhl.litespring.test.test;

import com.yhl.litespring.utils.ClassUtils;

import java.net.URL;

/**
 * Created by 44811 on 2018/6/17.
 */
public class BaseTest {
    protected String getFilePath(String fileName) {
        ClassLoader cl = ClassUtils.getDefaultClassLoader();
        URL resource = cl.getResource(fileName);
        return resource.getFile();
    }
}
