package com.yhl.litespring.test.test.annotation;

import com.yhl.litespring.beans.factory.annotation.AnnotationAttributes;
import com.yhl.litespring.core.io.ClassPathResource;
import com.yhl.litespring.core.type.classreading.AnnotationMetaDataReadingVisitor;
import com.yhl.litespring.core.type.classreading.ClassMetaDataReadingVisitor;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.asm.ClassReader;

import java.io.IOException;

/**
 * Created by 44811 on 2018/7/29.
 */
public class ClassReaderTest {
    @Test
    public void testGetClassMetaData() throws IOException {
        ClassPathResource resource = new ClassPathResource("com/yhl/litespring/test/beans/service/GoodsServiceV2.class");
        ClassReader reader = new ClassReader(resource.getInputStream());
        ClassMetaDataReadingVisitor visitor = new ClassMetaDataReadingVisitor();
        reader.accept(visitor, ClassReader.SKIP_DEBUG);
    }

    @Test
    public void testGetAnnotationMetaData() throws IOException {
        ClassPathResource resource = new ClassPathResource("com/yhl/litespring/test/beans/service/GoodsServiceV2.class");
        ClassReader reader = new ClassReader(resource.getInputStream());
        AnnotationMetaDataReadingVisitor visitor = new AnnotationMetaDataReadingVisitor();
        reader.accept(visitor, ClassReader.SKIP_DEBUG);

        String annotation = "com.yhl.litespring.stereotype.Component";
        Assert.assertTrue(visitor.hasAnnotation(annotation));

        AnnotationAttributes attributes = visitor.getAnnotationAttributes(annotation);
        Assert.assertEquals("goodsServiceV2", attributes.get("value"));
    }
}
