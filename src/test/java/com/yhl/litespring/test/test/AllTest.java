package com.yhl.litespring.test.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by 44811 on 2018/7/8.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ApplicationContextTest.class, BeanDefinitionTest.class, BeanFactoryTest.class,
                     CustomBooleanTest.class, CustomNumberTest.class, ResourceTest.class, ConstructorResolverTest.class})
public class AllTest {
}
