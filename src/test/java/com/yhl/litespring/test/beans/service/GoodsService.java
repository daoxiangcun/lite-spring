package com.yhl.litespring.test.beans.service;

import com.yhl.litespring.test.beans.dao.GoodsDAO;
import com.yhl.litespring.test.beans.model.Good;

/**
 * Created by 44811 on 2018/6/24.
 */
public class GoodsService {
    private GoodsDAO goodsDAO;
    private String wareHouse;       // 仓库名称
    private int totalGoods;

    public GoodsService() {

    }

    public GoodsService(GoodsDAO goodsDAO, String wareHouse, int totalGoods) {
        this.goodsDAO = goodsDAO;
        this.wareHouse = wareHouse;
        this.totalGoods = totalGoods;
    }

    public GoodsService(GoodsDAO goodsDAO, String wareHouse) {
        this.goodsDAO = goodsDAO;
        this.wareHouse = wareHouse;
    }

    public Good getGoodInfoByGoodId(String goodId) {
        return new Good();
    }

    public GoodsDAO getGoodsDAO() {
        return goodsDAO;
    }

    public void setGoodsDAO(GoodsDAO goodsDAO) {
        this.goodsDAO = goodsDAO;
    }

    public String getWareHouse() {
        return wareHouse;
    }

    public void setWareHouse(String wareHouse) {
        this.wareHouse = wareHouse;
    }

    public int getTotalGoods() {
        return totalGoods;
    }

    public void setTotalGoods(int totalGoods) {
        this.totalGoods = totalGoods;
    }
}
