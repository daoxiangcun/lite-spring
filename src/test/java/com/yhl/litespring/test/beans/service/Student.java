package com.yhl.litespring.test.beans.service;

/**
 * Created by 44811 on 2018/6/20.
 */
public class Student {
    private static int count = 0;
    public Student() {
        count++;
    }

    public int getCount() {
        return count;
    }
}
