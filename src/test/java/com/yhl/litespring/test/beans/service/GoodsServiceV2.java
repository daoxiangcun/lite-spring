package com.yhl.litespring.test.beans.service;

import com.yhl.litespring.beans.factory.annotation.Autowried;
import com.yhl.litespring.stereotype.Component;
import com.yhl.litespring.test.beans.dao.GoodsDAOV2;

/**
 * Created by 44811 on 2018/7/29.
 */
@Component(value = "goodsServiceV2")
public class GoodsServiceV2 {
    @Autowried
    GoodsDAOV2 goodsDAOV2;
    String wareHouse;       // 仓库名称
    int totalGoods;

    public GoodsDAOV2 getGoodsDAOV2() {
        return goodsDAOV2;
    }

    public void setGoodsDAOV2(GoodsDAOV2 goodsDAOV2) {
        this.goodsDAOV2 = goodsDAOV2;
    }

    public String getWareHouse() {
        return wareHouse;
    }

    public void setWareHouse(String wareHouse) {
        this.wareHouse = wareHouse;
    }

    public int getTotalGoods() {
        return totalGoods;
    }

    public void setTotalGoods(int totalGoods) {
        this.totalGoods = totalGoods;
    }
}
